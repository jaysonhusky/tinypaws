# TinyPAWS


TinyPAWS is a Free Open Source Software (F.O.S.S) for users to publish an online image gallery.
	
	Version 1.0a:
		* Support for JPG, PNG & BMP files is currently available.
		* MOV, MP4 and FLV files to be supported from version 2.1a
		
	
	Current Features:
	
		* Small, But useful admin panel
		* MySQL connectivity
		* Database-less usage
		* Automatic thumbnailer
		
	Known Issues:
	
		* Table based design
		* Partial support to Internet Explorer & Safari
	
	
# Licensing 

TinyPaws is licensed under Creative Commons BY 3.0 http://creativecommons.org/licenses/by/3.0/# TinyPaws README
# TinyPaws README
