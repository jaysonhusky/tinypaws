<?php
	/*
	#####################################################################################
	############################ TINYPAWS CONFIGURATION FILE ############################
	####### For full documentation, visit; wwww.jaysonhuskystudios.co.nr/tinypaws #######
	#####################################################################################
	*/

// Stop direct access to file!
	if(!defined('isinc')) {
		die('Direct access not permitted');
	}

	
	/*
		YOU CAN EDIT FROM BELOW THIS LINE!
	*/

// TinyPaws Configuration	
	define('TinyPaws', 'mysql'); // Whether or not the gallery is to use MySQL or No Database
	
	$galleryroot = $_SERVER['PHP_SELF']; // Root Location of gallery (change this if gallery is included as a file, leave if used as standalone)
	$TPDomain = "www.example.com"; // Domain in which this is installed.
	$gallerythumbs = "thumbs"; // Thumbnails location, typically this is ok as it is.
	$thumbsize_height = "150"; // Size in pixels
	$thumbsize_width = "150"; // Size in pixels
	$ArticleDisplayMethod = "lightbox"; // If the image should be shown on its own or in a container such as lightbox or fancybox.
	$ThumbCompressionRatio = "80";
	$FancyUrls = "enabled"; // Search Engine friendly URL's or bad urls?
	
	
// Administrative Configuration

	$admin_panel_enabled = "no"; 
	$user_ad = "admin";
	$pass_ad = "password";
	

// Gallery Configuration
	
	$ThumbGen = "disabled"; // Automatic Thumbnail Generator. (Will not generate thumbnails for PSD files)
	$AllowedFiles = array("png","jpg","gif","psd");  // The Allowed FileTypes!
	$ItemsPerRow = "10"; // How many images per row?
	


	/*
		DATABASE CONFIGURATION! 
		NOTE! IF THE CONFIGURATION ABOVE IS SET TO "NODB" THEN THESE SETTINGS DO NOT TAKE EFFECT
	*/
	
	// The backend engine library.
    define('DB_ENGINE', 'mysql');

    // If you're using the default MySQL server, change the below to
    // match your settings.
    define('MYSQL_USERNAME', '');
    define('MYSQL_PASSWORD', '');
    define('MYSQL_HOSTNAME', '');
    define('MYSQL_DATABASE', '');
    define('MYSQL_PREFIX', 'tinypaws_');

	/*
		DO NOT EDIT BELOW THIS LINE.
		SERIOUSLY IT TOOK A LOT OF WORK TO GET IT WORKING RIGHT!
	*/


function HandleError($errmsg) {
	print '<!DOCTYPE html>
	<html>
	<head>
	<title>Error</title>
	<meta charset="utf-8">
	</head>
	<body>
	'.$errmsg.'
	</body>
	</html>
';
}
	
	
// Manual Thumb Generation	
function thumbgen($iPath, $tPath, $tWidth ) {
	$dir = opendir($iPath);
	while (false !== ($fname = readdir($dir))) {
	$info = pathinfo($iPath . $fname);
	if ( strtolower($info['extension']) == 'jpg' ) {
		if(file_exists("{$fname}")) {
			//Thumbnail Exists, Do Not Re-create
		}
		else {
			$img = imagecreatefromjpeg( "{$iPath}{$fname}" );
			$width = imagesx($img);
			$height = imagesy($img);
			$new_width = $thumbWidth;
			$new_height = floor( $height * ($thumbWidth / $width) );
			$tmp_img = imagecreatetruecolor($new_width, $new_height);
			imagecopyresized($tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
			imagejpeg( $tmp_img, "{$tPath}{$fname}" );
		}
	}
	}
	closedir( $dir );
}
function WebMoveTo($url) {
	$url = strtolower($url);
	header("Location: $url");
}
	
function Passthrough($var) {
	$AddSomeSalt = "xJAK12ki-ASKM1"; // A random string, change this if you install TinyPaws!
	$var = htmlentities($var); // Remove Tags (Nullifies any rogue code)
	$var = stripslashes($var); // Remove Slashes (Removes slashes, which are often dependant for code execution)
	$var = strtolower($var); // Down to lowercase, makes it easier to play with
	$var = $var.$AddSomeSalt; // Add some ecryption
	$var = sha1($var); // Hide the contents.
	return $var; // Send it back to the user!
}
function tpisadmin() {
	$FormAUser = htmlentities($_POST['username']); $FormAPass = htmlentities($_POST['password']);
	if($admin_panel_enabled == "yes") {
		If(isset($_POST['login'])) {
			If(($_POST['password']==$admin_password) AND ($_POST['username']==$admin_username)) {
				If(($FormAUser == $user_ad) && ($FormAPass == $pass_ad)) {
					setcookie("TinyPawsAP",Passthrough($logindetails),time()+60*60*24,"/",".".$TPDomain."",0,true);
					sleep(1);
					WebMoveTo("/administration");
					exit;
				}
			}
		}
	}
}


// Check if GD (The Image Library) is installed to web server!			
if (extension_loaded('gd') && function_exists('gd_info')) {
    // GD IS Installed, Do Nothing
}
else {
	HandleError("GD Image Library Is Not Installed"); // Report error to user.
}	
?>